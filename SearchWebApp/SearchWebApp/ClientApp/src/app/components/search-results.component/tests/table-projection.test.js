import { TableProjection } from "../table-projection";


describe('SearchResults projection', () => {
    const getDescription = function (item){
        return `${item.fieldA}.${item.fieldB}.${item.fieldC}`;
    }
    const sample_items =[
      { fieldA: '1', fieldB: '1', fieldC: '3' },
      { fieldA: '1', fieldB: '2', fieldC: '2' },
      { fieldA: '1', fieldB: '3', fieldC: '1' },
      { fieldA: '2', fieldB: '1', fieldC: '3' },
      { fieldA: '2', fieldB: '2', fieldC: '2' },
      { fieldA: '2', fieldB: '3', fieldC: '1' },
      { fieldA: '3', fieldB: '1', fieldC: '3' },
      { fieldA: '3', fieldB: '2', fieldC: '2' },
      { fieldA: '3', fieldB: '3', fieldC: '1' },
    ];

    describe('happy paths', () => {

        const headers = [
            {
                from: 'fieldA',
                text: 'A_label'
            },
            {
                from: 'fieldB',
                text: 'B_label'
            },
            {
                from: 'fieldC',
                text: 'C_label'
            }
        ];

        describe('by fieldA', () => {
            it('initial value until sorting is not provided', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '1.1.3',
                    '1.2.2',
                    '1.3.1',
                    '2.1.3',
                    '2.2.2',
                    '2.3.1',
                    '3.1.3',
                    '3.2.2',
                    '3.3.1',
                ]);
            });
    
            it('asc sort by fieldA', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[0].from);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '1.1.3',
                    '1.2.2',
                    '1.3.1',
                    '2.1.3',
                    '2.2.2',
                    '2.3.1',
                    '3.1.3',
                    '3.2.2',
                    '3.3.1',
                ]);
            });
    
            it('desc sort by fieldA', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[0].from);
                projection.toggleSorting(headers[0].from);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '3.1.3',
                    '3.2.2',
                    '3.3.1',
                    '2.1.3',
                    '2.2.2',
                    '2.3.1',
                    '1.1.3',
                    '1.2.2',
                    '1.3.1',
                ]);
            });
    
            it('asc sort by fieldA switched back', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[0].from);
                projection.toggleSorting(headers[0].from);
                projection.toggleSorting(headers[0].from);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '1.1.3',
                    '1.2.2',
                    '1.3.1',
                    '2.1.3',
                    '2.2.2',
                    '2.3.1',
                    '3.1.3',
                    '3.2.2',
                    '3.3.1',
                ]);
            });
        });

        describe('by fieldB', () => {
            it('asc sort by fieldB', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[1].from);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '1.1.3',
                    '2.1.3',
                    '3.1.3',
                    '1.2.2',
                    '2.2.2',
                    '3.2.2',
                    '1.3.1',
                    '2.3.1',
                    '3.3.1',
                ]);
            });
    
            it('desc sort by fieldB', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[1].from);
                projection.toggleSorting(headers[1].from);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '1.3.1',
                    '2.3.1',
                    '3.3.1',
                    '1.2.2',
                    '2.2.2',
                    '3.2.2',
                    '1.1.3',
                    '2.1.3',
                    '3.1.3',
                ]);
            });
    
            it('asc sort by fieldB switched back', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[1].from);
                projection.toggleSorting(headers[1].from);
                projection.toggleSorting(headers[1].from);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '1.1.3',
                    '1.2.2',
                    '1.3.1',
                    '2.1.3',
                    '2.2.2',
                    '2.3.1',
                    '3.1.3',
                    '3.2.2',
                    '3.3.1',
                ]);
            });
        });

        describe('composition', () => {
            it('asc sort by fieldC', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[2].from);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '1.3.1',
                    '2.3.1',
                    '3.3.1',
                    '1.2.2',
                    '2.2.2',
                    '3.2.2',
                    '1.1.3',
                    '2.1.3',
                    '3.1.3',
                ]);
            });

            it('asc sort by fieldC then desc by fieldA', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[2].from);
                projection.toggleSorting(headers[0].from);
                projection.toggleSorting(headers[0].from);
                const sorted = projection.getSortedContent();
                expect(sorted.content.map(i => getDescription(i))).toEqual([
                    '3.3.1',
                    '2.3.1',
                    '1.3.1',
                    '3.2.2',
                    '2.2.2',
                    '1.2.2',
                    '3.1.3',
                    '2.1.3',
                    '1.1.3',
                ]);
            });
        });

        describe('headers reordering', () => {
            it('fieldC in top', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[2].from);
                const sorted = projection.getSortedContent(true);
                expect(sorted.headers.map(i => i.from)).toEqual([
                    'fieldC',
                    'fieldA',
                    'fieldB'
                ]);
            });

            it('fieldC then fieldB', () => {
                const projection = new TableProjection(headers);
                projection.updateContent(sample_items);
                projection.toggleSorting(headers[2].from);
                projection.toggleSorting(headers[1].from);
                const sorted = projection.getSortedContent(true);
                expect(sorted.headers.map(i => i.from)).toEqual([
                    'fieldC',
                    'fieldB',
                    'fieldA'
                ]);
            });
        });
    });
  });