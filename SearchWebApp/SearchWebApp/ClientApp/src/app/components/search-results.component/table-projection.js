import { sort } from "../../core/helpers/multiSort.provider";

export class TableProjection{

    _sortingFields = [];

    _sortingToggleOrder = [
        true,
        false,
        null
    ];

    constructor(headers){
        this._headers = headers;
    }

    toggleSorting(colKey){
        const colIndex = this._headers.findIndex(col => col.from === colKey);
        const index = this._sortingFields.findIndex(c => c.index == colIndex);

        const isItemFound = index != -1;

        if (!isItemFound){
            const newDirection = this._sortingToggleOrder[0];

            if (newDirection != null){
                this._sortingFields.push({
                    index: colIndex,
                    direction: newDirection
                });
            }            
        }
        else {
            const currDirectionIndex = this._sortingToggleOrder.findIndex(i => i == this._sortingFields[index].direction);
            const nextDirectionIndex = 
                currDirectionIndex < (this._sortingToggleOrder.length - 1) ? 
                    currDirectionIndex + 1 :
                    0;
            const newDirection = this._sortingToggleOrder[nextDirectionIndex];

            if (newDirection != null){
                this._sortingFields[index].direction = newDirection;
            }
            else{
                this._sortingFields.splice(index, 1);
            }
        }

        return this;
    }

    updateContent(items){
        this._items = items;

        return this;
    }

    getSortedContent(reorderHeaders = false){

        const mappedHeaders = this._headers.slice().map((col, colIndex) =>{
            const indexInSorting = this._sortingFields.slice().reverse().findIndex(c => c.index == colIndex);
            if (indexInSorting != -1){
                col.sort = this._sortingFields.slice().reverse()[indexInSorting].direction;
            }
            else {
                col.sort = null;
            }
            return {
                indexInSorting,
                col
            };
        });

        if (reorderHeaders){
            mappedHeaders.sort((a, b) => {
                const r = b.indexInSorting - a.indexInSorting;
                return r;
            });
        }

        const headers = mappedHeaders.map(h => h.col);

        if (this._items == null || this._items.length === 0){
            return {
                headers,
                content: this._items.slice()
            };
        }

        const sortingStrategy = this._sortingFields.map(f =>({
                func: i => i[this._headers[f.index].from],
                desc: !f.direction}));

        const mapped = sort(this._items).byMultiple(sortingStrategy).result;

        return {
            headers,
            content: mapped
        };
    }
}