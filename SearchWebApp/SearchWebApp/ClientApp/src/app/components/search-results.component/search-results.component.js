import { ComponentBase } from "../component.base";
import { TableProjection } from "./table-projection";

export class SearchResultsComponent extends ComponentBase {

    _nativeLockerContainer = [];
    _nativeHeadersContainer;
    _nativeElementsContainer = [];
    _container;

    _tableProjection;
    _reorderHeaders;

    constructor(headers, reorderHeaders = false){
        super();

        this._tableProjection = new TableProjection(headers.map(h => ({...h, sort:null})));
        this._reorderHeaders = reorderHeaders;
    }

    updateContent(items){
        //remove content
        this.removeElements(this._container);

        if (!items){
            //remove headers
            this.removeHeaders(this._container);
            this._tableProjection.updateContent([]);
            //show error message
        }
        else if (items.length == 0){
            this._tableProjection.updateContent(items)
            //show items not found
        }
        else{
            const updated = this._tableProjection
                .updateContent(items)
                .getSortedContent(this._reorderHeaders);

            if (!!this._nativeHeadersContainer){
                //reorder headers
                this.reorderHeaders(updated.headers);
            }
            else {
                this.renderHeaders(this._container, updated.headers);
            }
            
            //render content
            this.renderContent(this._container, updated.content, updated.headers);
        }
    }

    toggleSorting(colKey){
        const updated = this._tableProjection
            .toggleSorting(colKey)
            .getSortedContent(this._reorderHeaders);

        this.reorderHeaders(updated.headers);

        //remove content
        this.removeElements(this._container);
        //render content
        this.renderContent(this._container, updated.content, updated.headers);
    }

    toggleColumnsFreeze(locker){
        this._reorderHeaders = !this._reorderHeaders;
        this.setLockerStyling(locker);               
    }

    setLockerStyling(locker){
        if (!this._reorderHeaders){
            locker.classList.remove('locked');
        }
        else{
            locker.classList.add('locked');
        } 
    }

    renderHeaders(container, headers){
        const lastIndex = headers.length - 1;
        const nativeHeaderElements = headers.map((h, index) => {
            const wrapper = document.createElement('div');

            const label = document.createElement('div');
            label.textContent = h.text;

            const sort = document.createElement('div');
            sort.classList.add('sort-icon-container');
            const sortIcon = document.createElement('div');
            sortIcon.classList.add('icon');
            sort.appendChild(sortIcon);

            wrapper.appendChild(label);
            wrapper.appendChild(sort);

            wrapper.classList.add(`row-header`);
            wrapper.classList.add(`col-${index}`);

            wrapper.classList.remove('sort-asc', 'sort-desc');

            label.onclick = () => this.toggleSorting(h.from);
            sort.onclick = () => this.toggleSorting(h.from);

            return {
                from: h.from,
                sort: h.sort,
                element: wrapper
            };
        });

        this._nativeHeadersContainer = nativeHeaderElements;
        this._nativeHeadersContainer.forEach(e => container.appendChild(e.element));
        

        for (let i = 0; i < headers.length - 2; i++) {
            // because items are contained automatically
            const stub = document.createElement('div');
            container.appendChild(stub);
            this._nativeLockerContainer.push(stub);          
        }

        const locker = document.createElement('div');
        locker.classList.add('locker-icon-container');
        locker.classList.add(`row-prepend`);
        locker.classList.add(`col-${headers.length - 1}`);
        const lockerIcon = document.createElement('div');
        lockerIcon.classList.add('icon');
        locker.appendChild(lockerIcon);

        locker.onclick = () => this.toggleColumnsFreeze(locker);
        this.setLockerStyling(locker);

        container.appendChild(locker);

        this._nativeLockerContainer.push(locker);

        this._nativeHeadersContainer.forEach(e => container.appendChild(e.element));
    }

    reorderHeaders(headers){
        this._nativeHeadersContainer.forEach((h, index) => {
            h.element.classList.forEach(c => {
                if (c.startsWith('col-')){
                    h.element.classList.remove(c);
                }
            });

            const matchedIndex = headers.findIndex(hc => hc.from === h.from);
            h.element.classList.add(`col-${matchedIndex}`);
            h.element.classList.remove('sort-asc', 'sort-desc');
            if (headers[matchedIndex].sort === true){
                h.element.classList.add('sort-asc');
            }
            else if(headers[matchedIndex].sort === false){
                h.element.classList.add('sort-desc');
            }
        });
    }

    renderContent(container, items, headers){

        items.forEach(item => {
            headers.forEach((head, index) =>{
                const rdr = document.createElement('div');
                rdr.textContent = item[head.from];
                rdr.classList.add(`col-${index}`);
                container.appendChild(rdr);
                this._nativeElementsContainer.push(rdr);
            })
        });
    }

    removeHeaders(container){
        this._nativeHeadersContainer?.forEach(elem => container.removeChild(elem));
        this._nativeHeadersContainer = null;

        this._nativeLockerContainer?.forEach(elem => container.removeChild(elem));
        this._nativeLockerContainer = [];
    }

    removeElements(container){
        this._nativeElementsContainer?.forEach(elem => container.removeChild(elem));
        this._nativeElementsContainer = [];
    }

    render(replacedNode){
        const container = document.createElement('div');
        container.classList.add('search-results-container');

        this._container = container;
        super.render(replacedNode, container);
    }
}