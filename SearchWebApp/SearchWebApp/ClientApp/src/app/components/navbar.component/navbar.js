import { Button } from "../button.component/button.compnent";
import { ComponentBase } from "../component.base";

export class NavBar extends ComponentBase{
    constructor(items){
        super();
        this.items = items;
    }

    render(replacedNode){
        const barWrapper = document.createElement('div');
        barWrapper.classList.add('nav-bar');

        this.appendToggle(barWrapper);
        this.appendNavItems(barWrapper);        

        super.render(replacedNode, barWrapper);
    }

    appendToggle(barWrapper){       

        const barController = document.createElement('div');
        barController.classList.add('nav-bar-controller');
        const controllerArrow = document.createElement('div');
        controllerArrow.classList.add('icon');

        barController.appendChild(controllerArrow);
        barWrapper.appendChild(barController);

        barController.onclick = () => barWrapper.classList.toggle('collapsed');
    }

    appendNavItems(barWrapper){
        const navButtons = [];

        const container = document.createElement('ul');
        container.classList.add('nav-items-container');
        
        this.items.forEach(item => {
            const elementWrapper = document.createElement('li');
            elementWrapper.classList.add('btn-wrapper');
            
            const button = new Button(
                item.label,
                (newState) =>{
                    navButtons.forEach(b => b.setState(false));
                    item.callback(newState);
                },
                false);
            button.render(elementWrapper);
            navButtons.push(button);
            
            container.appendChild(elementWrapper);
        });

        barWrapper.appendChild(container);
    }
}