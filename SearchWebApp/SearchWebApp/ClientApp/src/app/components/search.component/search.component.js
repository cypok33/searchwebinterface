import { ComponentBase } from "../component.base";

export class SearchComponent extends ComponentBase{
    constructor(label, buttonLabel, patten, errorMessage, searchCallback){
        super();
        this.label = label;
        this.buttonLabel = buttonLabel;
        this.patten = patten;
        this.errorMessage = errorMessage;
        this.searchCallback = searchCallback;
    }

    render(replacedNode){
        const form = document.createElement('form');
        form.classList.add('search-component');
        form.onsubmit = (function(event) { 
            this.searchCallback(this.valueInput.value);
            event.preventDefault();
            return false;
        }).bind(this);

        const label = document.createElement('label');
        label.classList.add('text-label');
        label.innerHTML = this.label;

        const input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('name', `search_for_${this.label.replace(/[^a-zA-Z]+/, '')}`);
        input.setAttribute('required', '');
        input.setAttribute('pattern', this.patten);
        input.setAttribute('title', this.errorMessage);
        input.setAttribute('oninvalid', `setCustomValidity('${this.errorMessage}')`);
        input.setAttribute('oninput', "setCustomValidity('')");
        input.classList.add('text-input');
        this.valueInput = input;        

        const button = document.createElement('input');
        button.setAttribute('type', 'submit');
        button.value = this.buttonLabel;
        button.classList.add('btn');

        form.appendChild(label);
        form.appendChild(input);
        form.appendChild(button);

        super.render(replacedNode, form);
    }
}