export class ComponentBase{
    render(replacedNode, content){
        replacedNode.firstChild ? 
            replacedNode.firstChild.replaceWith(content) :
            replacedNode.appendChild(content);
    }

    renderAppend(replacedNode, content){
        replacedNode.appendChild(content);
    }
}