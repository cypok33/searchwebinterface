import { ComponentBase } from "../component.base";

export class Button extends ComponentBase{
    _label;
    _onClickCallback;
    _state;

    constructor(label, callback, initial = false, forSubmit = false){
        super();
        this._label = label;
        this._onClickCallback = callback;
        this._state = initial;
        this._forSubmit = forSubmit;
    }

    setState(newState){
        this._state = newState;
        this._nativeElement.classList.remove('active');

        if(newState){
            this._nativeElement.classList.add('active');
        }
    }

    get state(){
        return this._state;
    }

    generateNode(){
        const button = document.createElement('div');
        button.classList.add('btn');
        
        if(this._forSubmit){
            button.setAttribute('type', 'submit');
        }
        
        button.onclick = () =>{
            if(this._state != null){
                const newState = !this.state;
                if(this._onClickCallback != null){
                    this._onClickCallback(newState);
                }            
                this.setState(newState);
            }
        };

        const label = document.createElement('a');
        label.textContent = this._label;        
        button.appendChild(label);

        this._nativeElement = button;

        return button;
    }

    render(replacedNode){        
        super.render(replacedNode, this.generateNode());
    }

    renderAppend(replacedNode){        
        super.renderAppend(replacedNode, this.generateNode());
    }
} 