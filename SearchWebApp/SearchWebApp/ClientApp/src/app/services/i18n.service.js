import { Locales, messagesJson } from "../parameters/localized.strings";

const defaultLocale = Locales.enUs;

const currentLocale = navigator.language;

const messages = JSON.parse(messagesJson);

const textSet = messages.find(m => currentLocale.toLowerCase().startsWith(m.locale)) ??
                messages.find(m => m.locale === defaultLocale);

export function loc(key){
    return textSet[key] ?? key;
}