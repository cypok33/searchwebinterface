import {RoutingService} from './routing.service'
import {SearchService} from './search.service';

export class SearchApplication{
    constructor(){
        this.routingService = new RoutingService();
        this.searchService = new SearchService();

        this.routingService.ipSearchCallback = async value => await this.searchService.ipSearchAsync(value);
        this.routingService.citySearchCallback = async value => await this.searchService.citySearchAsync(value);
        this.searchService.pushSearchResultCallback = this.routingService.displaySearchResult.bind(this.routingService);

        this.routingService.init();
    }
}