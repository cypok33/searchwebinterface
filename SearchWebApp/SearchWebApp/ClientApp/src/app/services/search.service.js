import { ApiAcessor } from "../core/helpers/api.accessor.mockable";

export class SearchService {

    constructor() {
        this.apiAccessor = new ApiAcessor();
    }

    async ipSearchAsync(parameters){
        const result = await this.apiAccessor.ipSearchAsync(parameters);
        this.pushSearchResultCallback(result);
    }

    async citySearchAsync(parameters){
        const result = await this.apiAccessor.citySearchAsync(parameters);
        this.pushSearchResultCallback(result);
    }

    pushSearchResultCallback;
}