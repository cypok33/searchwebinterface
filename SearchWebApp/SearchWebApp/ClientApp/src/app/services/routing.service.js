import { SearchComponent } from "../components/search.component/search.component";
import { NavBar } from "../components/navbar.component/navbar";
import {SearchResultsComponent} from '../components/search-results.component/search-results.component';
import { loc } from "./i18n.service";
import { Labels } from "../parameters/localized.strings";

export class RoutingService{

    modes = [
        {
            label: loc(Labels.ip_search_header),
            preselected: true,
            callback:((selected) => this.injectSearchComponent(
            !selected ? null :{
                searchLabel: loc(Labels.ip_search_label),
                buttonLabel: loc(Labels.search_button_label),
                mask: "((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}",
                validationErrorMessage: loc(Labels.ip_validation_msg),
                searchCallback: this.ipSearchCallback.bind(this)
            }))
        },
        {
            label: loc(Labels.city_search_header),
            callback:((selected) => this.injectSearchComponent(
            !selected ? null :{
                searchLabel: loc(Labels.city_search_label),
                buttonLabel: loc(Labels.search_button_label),
                mask: "^[a-zA-Z0-9_ ]+$",
                validationErrorMessage: loc(Labels.city_validation_msg),
                searchCallback: this.citySearchCallback.bind(this)
            }))
        }
    ];


    searchResultComponent;

    init(){
        document.title = loc(Labels.doc_title);

        const navBarContainer = document.getElementsByClassName('nav-bar-container')[0];
        const navBar = new NavBar(this.modes);
        navBar.render(navBarContainer);

        const searchResultContainer = document.getElementsByClassName('search-result-container')[0];
        this.searchResultComponent = new SearchResultsComponent([
            {text: loc(Labels.org_col_name), from: 'organization'},
            {text: loc(Labels.country_col_name), from: 'country'},
            {text: loc(Labels.region_col_name), from: 'region'},
            {text: loc(Labels.postal_col_name), from: 'postal'},
            {text: loc(Labels.city_col_name), from: 'city'}
        ]);
        this.searchResultComponent.render(searchResultContainer);
    }


    injectSearchComponent(parameters){
        const searchContainer = document.getElementsByClassName('search-action-container')[0];

        if(parameters == null){
            searchContainer?.firstChild ? searchContainer?.removeChild(searchContainer?.firstChild) : null;
            this.searchResultComponent.cleanUp();
            return;
        }

        const searchComponent = new SearchComponent(
            parameters.searchLabel,
            parameters.buttonLabel,
            parameters.mask,
            parameters.validationErrorMessage,
            parameters.searchCallback);
        
        searchComponent.render(searchContainer);
    }
    
    displaySearchResult(searchResult){
        this.searchResultComponent.updateContent(searchResult);
    }

    ipSearchCallback;
    citySearchCallback;
}