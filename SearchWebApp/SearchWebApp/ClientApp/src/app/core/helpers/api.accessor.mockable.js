import * as configuration from "./../../parameters/connectionParameters"

export class ApiAcessor{

    async ipSearchAsync(ip) {
        const response = await this.searchAsync(configuration.IP_SEARCH_ENDPOINT, {ip});

        return [{
            country: response.postalAddress.country,
            region: response.postalAddress.region,
            postal: response.postalAddress.postal,
            city: response.postalAddress.city,
            organization: response.organizationName,
            latitude: response.geoLocation.latitude,
            longitude: response.geoLocation.longitude
        }];
    }
    
    async citySearchAsync(city) {
        const response = await this.searchAsync(configuration.CITY_SEARCH_ENDPOINT, {city});

        return response.map(r =>({
            country: r.postalAddress.country,
            region: r.postalAddress.region,
            postal: r.postalAddress.postal,
            city: r.postalAddress.city,
            organization: r.organizationName,
            latitude: r.geoLocation.latitude,
            longitude: r.geoLocation.longitude
        }));
    }
    
    async searchAsync(endpoint, params){
        // const url = new URL(configuration.API_URL + endpoint);
        // Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

        const query = new URLSearchParams(params).toString();
        const url = configuration.API_URL + endpoint;
        const parametrized = url +(query.length > 0 ? ('?' + query) : '');
        const response =  await fetch(parametrized);
    
        if(!response.ok){
            return null;
        }
    
        return response.json();
    }
}