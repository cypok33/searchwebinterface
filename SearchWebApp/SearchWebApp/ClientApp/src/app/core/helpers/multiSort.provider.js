export function sort(array){
    return new sorter(array);
}

export class sorter{

    array;

    constructor(array)
    {
        this.array = array;
    }

    comparers = [];

    by(func){
        this.comparers.push({direct: true, func: func});
        return this;
    }

    byDesc(func){
        this.comparers.push({direct: false, func: func});
        return this;
    }

    byMultiple(multipleSorters){
        multipleSorters.forEach(s => this.comparers.push({func: s.func, direct: !(s.desc ?? false)}));
        return this;
    }

    get result(){
        if (!this.array || this.array.length <= 1){
            return this.array;
        }

        if (!this.comparers || this.array.comparers <= 0){
            return this.array;
        }

        const revertedComparers = this.comparers.slice().reverse();
        const copy = this.array.slice();

        const sorted = copy.sort((a, b) => {
            const cmp = revertedComparers.slice();
            let result = 0;
            while(cmp.length){
                const singleCmp = cmp.pop();
                result =+ sortFromComparer(a, b, singleCmp) * (singleCmp.direct ? 1 : -1);
                if(result != 0){
                    break;
                }
            }
            return result;
        });

        return sorted;
    }
}

function sortFromComparer(a, b, comparer){
    //items check
    if(a == null && b == null){
        return 0;
    }
    else if(a == null){
        return 1;
    }
    else if(b == null){
        return -1;
    }

    //selected checks
    if(comparer.func(a) == null && comparer.func(b) == null){
        return 0;
    }
    else if(comparer.func(a) == null){
        return 1;
    }
    else if(comparer.func(b) == null){
        return -1;
    }

    //selected comparsion
    if(comparer.func(a) > comparer.func(b)){
        return 1;
    }
    else if(comparer.func(a) < comparer.func(b)){
        return -1;
    }
    else {
        return 0;
    }
}