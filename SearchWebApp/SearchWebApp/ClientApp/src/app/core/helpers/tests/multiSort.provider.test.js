import { sort } from "../multiSort.provider";

describe('Multisorting Extension', () => {
  const sample_4 =[
    {
        fieldA: '1',
        fieldB: '1',
        index: 0,
        desc: '1.1'
    },
    {
        fieldA: '1',
        fieldB: '2',
        index: 1,
        desc: '1.2'
    },
    {
        fieldA: '2',
        fieldB: '1',
        index: 2,
        desc: '2.1'
    },
    {
        fieldA: '2',
        fieldB: '2',
        index: 3,
        desc: '2.2'
    },
  ];

  it('check aux testing mapping for asserts', () => {
    expect(sample_4.map(i => i.desc)).toEqual(['1.1', '1.2', '2.1', '2.2']);
  });

  describe('happy paths', () => {    
  
    it('single ascending', () => {
      expect(sort(sample_4).by(i => i.fieldA).result.map(i => i.desc)).toEqual(['1.1', '1.2', '2.1', '2.2']);
    });
  
    it('double ascending', () => {
      expect(sort(sample_4).by(i => i.fieldA).by(i => i.fieldB).result.map(i => i.desc)).toEqual(['1.1', '1.2', '2.1', '2.2']);
    });
  
    it('single descending', () => {
      expect(sort(sample_4).byDesc(i => i.fieldA).result.map(i => i.desc)).toEqual(['2.1', '2.2', '1.1', '1.2']);
    });
  
    it('double descending', () => {
      expect(sort(sample_4).byDesc(i => i.fieldA).byDesc(i => i.fieldB).result.map(i => i.desc)).toEqual(['2.2', '2.1', '1.2', '1.1']);
    });
  
    it('asc desc', () => {
      expect(sort(sample_4).by(i => i.fieldA).byDesc(i => i.fieldB).result.map(i => i.desc)).toEqual(['1.2', '1.1', '2.2', '2.1']);
    });
  
    it('desc asc', () => {
      expect(sort(sample_4).byDesc(i => i.fieldA).by(i => i.fieldB).result.map(i => i.desc)).toEqual(['2.1', '2.2', '1.1', '1.2']);
    });

    it('asc revert', () => {
      expect(sort(
        [
          {
              fieldA: 0,
              desc: '1.1'
          },
          {
              fieldA: -1,
              desc: '1.2'
          }
        ]
      ).by(i => i.fieldA).result.map(i => i.desc)).toEqual(['1.2', '1.1']);
    });

    it('desc revert', () => {
      expect(sort(
        [
          {
              fieldA: -1,
              desc: '1.1'
          },
          {
              fieldA: 0,
              desc: '1.2'
          }
        ]
      ).byDesc(i => i.fieldA).result.map(i => i.desc)).toEqual(['1.2', '1.1']);
    });

    it('multi desc ommitted', () => {
      expect(sort(sample_4)
        .byMultiple([
          {func: i => i.fieldA},
          {func: i => i.fieldB}
        ])
        .result.map(i => i.desc)).toEqual(['1.1', '1.2', '2.1', '2.2']);
    });

    it('multi desc explicit desc', () => {
      expect(sort(sample_4)
        .byMultiple([
          {func: i => i.fieldA, desc: false},
          {func: i => i.fieldB, desc: true}
        ])
        .result.map(i => i.desc)).toEqual(['1.2', '1.1', '2.2', '2.1']);
    });
  });

  const weird_samples =[
    {
        fieldA: '1',
        index: 0,
        desc: '1.1'
    },
    {
        fieldA: null,
        fieldB: '2',
        index: 1,
        desc: '1.2'
    },
    {
        fieldA: undefined,
        fieldB: '1',
        index: 2,
        desc: '2.1'
    },
    {
        fieldA: '2',
        fieldB: '2',
        index: 3,
        desc: '2.2'
    },
  ];

  describe('exceptions', () => {

    it('null as sorting criteria', () => {
      expect(sort(
        [
          {
              fieldA: null,
              desc: '1.1'
          },
          {
              fieldA: '1',
              desc: '1.2'
          }]
      ).by(i => i.fieldA).result.map(i => i.desc)).toEqual(['1.2', '1.1']);
    });

    it('null as sorting criteria not switching', () => {
      expect(sort(
        [
          {
              fieldA: '1',
              desc: '1.1'
          },
          {
              fieldA: null,
              desc: '1.2'
          }]
      ).by(i => i.fieldA).result.map(i => i.desc)).toEqual(['1.1', '1.2']);
    });

    it('both are nulls', () => {
      expect(sort(
        [
          {
              fieldA: null,
              fieldB: 2,
              desc: '1.1'
          },
          {
              fieldA: null,
              fieldB: 1,
              desc: '1.2'
          }]
      ).by(i => i.fieldA).by(i => i.fieldB).result.map(i => i.desc)).toEqual(['1.2', '1.1']);
    });

    it('both are nulls not switching', () => {
      expect(sort(
        [
          {
              fieldA: null,
              desc: '1.1'
          },
          {
              fieldA: null,
              desc: '1.2'
          }]
      ).by(i => i.fieldA).result.map(i => i.desc)).toEqual(['1.1', '1.2']);
    });

    it('undefined as sorting criteria', () => {
      expect(sort(
        [
          {
              fieldA: undefined,
              desc: '1.1'
          },
          {
              fieldA: '2',
              desc: '1.2'
          }]
      ).by(i => i.fieldA).result.map(i => i.desc)).toEqual(['1.2', '1.1']);
    });

    it('field not exists', () => {
      expect(sort(
        [
          {
              fieldB: '1',
              index: 0,
              desc: '1.1'
          },
          {
              fieldA: '2',
              index: 1,
              desc: '1.2'
          }]
      ).by(i => i.fieldA).result.map(i => i.desc)).toEqual(['1.2', '1.1']);
    });

    it('item is null', () => {
      expect(sort(
        [
          null,
          {
              fieldA: '1',
              index: 0,
              desc: '1.1'
          }
        ]
      ).by(i => i.fieldA).result.map(i => i?.desc ?? null)).toEqual(['1.1', null]);
    });

    it('item is undefined', () => {
      expect(sort(
        [
          undefined,
          {
              fieldA: '1',
              index: 0,
              desc: '1.1'
          }
        ]
      ).by(i => i.fieldA).result.map(i => i?.desc ?? undefined)).toEqual(['1.1', undefined]);
    });

  });
});