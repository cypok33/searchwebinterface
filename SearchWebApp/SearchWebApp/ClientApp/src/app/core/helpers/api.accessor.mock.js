const testLength = 300;

export class ApiAcessor{
    ipSearchAsync(ip) {
        console.log(`mocked Ip search for [${ip}]`);
        return this.generateRandomResponse(testLength);
    }
    
    citySearchAsync(city) {
        console.log(`mocked City search for [${city}]`);
        return this.generateRandomResponse(testLength);
    }
    
    generateRandomItem(){
        // return {
        //     country:'',
        //     region:'',
        //     postal:'',
        //     city:'',
        //     organization:'',
        //     latitude:-33.829357,
        //     longitude:150.961761
        // };

        return {
            country: 'cou_' + Math.round((Math.random() * 100)).toString(),
            region: 'reg_' + Math.round((Math.random() * 100)).toString(),
            postal:'zip_' + Math.round((Math.random() * 100)).toString(),
            city: 'cit_' + Math.round((Math.random() * 100)).toString(),
            organization: 'org_' + Math.round((Math.random() * 100)).toString(),
            latitude: Math.random() * (90 - (-90)) + (-90),
            longitude:Math.random() * (180 - (-180)) + (-180)
        };
    }
    
    async generateRandomResponse(count){
        var items = [];
        for(var i = 0; i < count; i++) {
            items.push(this.generateRandomItem());
        }
        return items;
    }
}