export const Locales = Object.freeze({
    en: 'en',
    ru: 'ru'
});

export const Labels = Object.freeze({
    doc_title: 'title',
    search_button_label: 'search_button_label',

    ip_search_header: 'ip_search_header',
    ip_search_label: 'ip_search_label',
    ip_validation_msg: 'ip_validation_msg',
    city_search_header: 'city_search_header',
    city_search_label: 'city_search_label',
    city_validation_msg: 'city_validation_msg',

    org_col_name: 'org_col_name',
    country_col_name: 'country_col_name',
    region_col_name: 'region_col_name',
    postal_col_name: 'postal_col_name',
    city_col_name: 'city_col_name',
});

export const messagesJson = 
    `[
        {
            "locale": "${Locales.en}",

            "${Labels.doc_title}": "Org search",

            "${Labels.search_button_label}": "Search",

            "${Labels.ip_search_header}": "Search by IPv4",
            "${Labels.ip_search_label}": "Ip search",
            "${Labels.ip_validation_msg}": "Must be a valid IPv4 Address",
            "${Labels.city_search_header}": "Search by City",
            "${Labels.city_search_label}": "City search",
            "${Labels.city_validation_msg}": "Must be a valid City",
            
            "${Labels.org_col_name}": "Organization",
            "${Labels.country_col_name}": "Country",
            "${Labels.region_col_name}": "Region",
            "${Labels.postal_col_name}": "Postal",
            "${Labels.city_col_name}": "City"
        },
        {
            "locale": "${Locales.ru}",

            "${Labels.doc_title}": "Поиск организации",

            "${Labels.search_button_label}": "Поиск",

            "${Labels.ip_search_header}": "Поиск по IPv4",
            "${Labels.ip_search_label}": "Поиск по IP",
            "${Labels.ip_validation_msg}": "Введите валидный IPv4 адрес",
            "${Labels.city_search_header}": "Поиск по городу",
            "${Labels.city_search_label}": "Поиск по городу",
            "${Labels.city_validation_msg}": "Невалидное название города",

            "${Labels.org_col_name}": "Организация",
            "${Labels.country_col_name}": "Страна",
            "${Labels.region_col_name}": "Регион",
            "${Labels.postal_col_name}": "Почтовый индекс",
            "${Labels.city_col_name}": "Город"
        }
    ]`;