export const API_URL = '/API';
export const IP_SEARCH_ENDPOINT = '/ip/location';
export const CITY_SEARCH_ENDPOINT = '/city/locations';