const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = function(env) {
  var isMocked = env.MOCKED || 'mockable';
  return {
    entry: './src/app/index.js',
    output: {
      path: path.resolve(__dirname + '/../AspHostApplication/ClientApp/'),
      filename: 'bundle.js'
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'src/app/index.html'
      }),
      new webpack.NormalModuleReplacementPlugin(
        /(.*)mockable(\.*)/,
        function (resource) {
          resource.request = resource.request.replace(
            /mockable/,
            `${isMocked}`
          );
        }
      ),
      new MiniCssExtractPlugin(),
    ],
    devServer: {
      static: {
        directory: path.join(__dirname, 'public'),
      },
      compress: true,
      port: 9000,
    },
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin({
          minify: TerserPlugin.uglifyJsMinify,
          terserOptions: {},
        }),
      ],
    },
    module: {
      rules: [
        {
          test: /\.s[ac]ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
            "sass-loader",
          ],
        },
      ],
    },
  }
};