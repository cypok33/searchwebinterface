
using System.Linq;

using NUnit.Framework;

using Common.Tests;
using DTO.Database.Mapping;
using Services.Implementations;

namespace Services.UnitTests
{
    public class FileDBTests
    {
        // for simplification, used MemoryMapper instance instead of mock, and sample file instead of mocked data
        private readonly PhysicalFileDbConfiguration _dbConfig = new PhysicalFileDbConfiguration("geobase.dat");

        [Test]
        public void InitialAllocation_Success()
        {
            var db = new FileDB(new MemoryMapper(), _dbConfig);
            db.Init();

            Assert.NotNull(db);
        }

        [Test]
        [TestCase("A ")]
        public void SearchFileCheck_Success(string cityName)
        {
            var db = new FileDB(new MemoryMapper(), _dbConfig);
            db.Init();

            var searchResults = db.CitySearch(cityName).ToArray();

            Assert.AreEqual(159, searchResults.Length);
            Assert.IsTrue(searchResults.All(r => r.PostalAddress.City.Equals(cityName)));

            Assert.AreEqual("Itykorow Hux", searchResults[0].OrganizationName);
            Assert.AreEqual("OZY", searchResults[0].PostalAddress.Country);
            Assert.AreEqual(cityName, searchResults[0].PostalAddress.City);
            Assert.AreEqual("Ixo", searchResults[0].PostalAddress.Region);
            Assert.AreEqual("02995", searchResults[0].PostalAddress.Postal);
            Assert.Less(searchResults[0].GeoLocation.Latitude - 35.2323, 0.001);
            Assert.Less(searchResults[0].GeoLocation.Longitude - 107.5794, 0.001);
        }

        [Test]
        [TestCase("0.5.236.229", "Uqyp Syb Cupat R")]
        [TestCase("0.7.171.151", "Ikudexevu Ije Ifeluvy E")]
        [TestCase("0.7.171.255", "Ikudexevu Ije Ifeluvy E")]
        [TestCase("0.8.233.247", "Ikudexevu Ije Ifeluvy E")]
        [TestCase("127.0.0.0", "Imymucol Xy")]
        [TestCase("231.175.71.65", "A Uz Fap")]
        public void SearchIp_Success(string ipAddress, string organizationName)
        {
            var db = new FileDB(new MemoryMapper(), _dbConfig);
            db.Init();

            var searchResults = db.IpV4Search(ipAddress).ToArray();

            Assert.AreEqual(1, searchResults.Length);
            Assert.AreEqual(organizationName, searchResults.Single().OrganizationName);
        }

        [TestCase("231.176.8.210")]
        [TestCase("255.255.255.255")]
        public void SearchIp_NotFound(string ipAddress)
        {
            var db = new FileDB(new MemoryMapper(), _dbConfig);
            db.Init();

            var searchResults = db.IpV4Search(ipAddress).ToArray();

            Assert.AreEqual(0, searchResults.Length);
        }
    }
}