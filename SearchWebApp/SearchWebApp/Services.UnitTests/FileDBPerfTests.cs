﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using NUnit.Framework;

using Common.Tests;
using DTO;
using DTO.Database.Mapping;
using Services.Implementations;

namespace Services.UnitTests
{
    // this tests shouldn't be used for benchmarking, but only to spot significant changes of performance 'on hot'
    public class FileDBPerfTests
    {
        private readonly PhysicalFileDbConfiguration _dbConfig = new PhysicalFileDbConfiguration("geobase.dat");

        [Test]
        [TestCase(25d, 25d, 1)]
        [TestCase(25d, 25d, 2)]
        [TestCase(20d, 20d, 10)]
        public void InitialAllocation_Perf(double timeoutDebug, double timeoutRelease, int times)
        {
#if DEBUG
            var timeout = timeoutDebug;
#else
            var timeout = timeoutRelease;
#endif

            var sw = new Stopwatch();
            sw.Reset();
            sw.Start();

            var gcPinContainer = new FileDB[times];

            for (var i = 0; i < times; i++)
            {
                gcPinContainer[i] = new FileDB(new MemoryMapper(), _dbConfig);
                gcPinContainer[i].Init();
            }

            sw.Stop();

            Assert.Less(sw.ElapsedMilliseconds, timeout * times);
        }

        [Test]
        [TestCase(0.100d, 0.100d, 100, new string[] {"A ", "Ixiluza", "A Adapo Ade Ar X", "Yzyrinorijyqotok", "Yzi" })]
        public void SearchSingleCity_Perf(double timeoutPerInstanceDebug, double timeoutPerInstanceRelease, int times, string[] names)
        {
#if DEBUG
            var timeoutPerInstance = timeoutPerInstanceDebug;
#else
            var timeoutPerInstance = timeoutPerInstanceRelease;
#endif

            var db = new FileDB(new MemoryMapper(), _dbConfig);
            db.Init();

            var gcPinContainer = new List<Organization[]>();

            var sw = new Stopwatch();
            sw.Reset();
            sw.Start();

            for (var i = 0; i < times; i++)
            {
                foreach(var name in names)
                {
                    var searchResults = db.CitySearch(name).ToArray();
                    gcPinContainer.Add(searchResults);
                }
            }

            sw.Stop();

            Assert.Less(sw.ElapsedMilliseconds, timeoutPerInstance * times * names.Length);
        }

        [Test]
        [TestCase(0.050d, 0.050d, 100, new string[] { "0.5.236.229", "0.7.171.151", "0.8.233.247", "127.0.0.0", "255.255.255.255" })]
        public void SearchSingleIp_Perf(double timeoutPerInstanceDebug, double timeoutPerInstanceRelease, int times, string[] addresses)
        {
#if DEBUG
            var timeoutPerInstance = timeoutPerInstanceDebug;
#else
            var timeoutPerInstance = timeoutPerInstanceRelease;
#endif

            var db = new FileDB(new MemoryMapper(), _dbConfig);
            db.Init();

            var sw = new Stopwatch();
            sw.Reset();
            sw.Start();

            var gcPinContainer = new List<Organization[]>();

            for (var i = 0; i < times; i++)
            {
                foreach (var address in addresses)
                {
                    var searchResults = db.IpV4Search(address).ToArray();
                    gcPinContainer.Add(searchResults);
                }
            }

            sw.Stop();

            Assert.Less(sw.ElapsedMilliseconds, timeoutPerInstance * times * addresses.Length);
        }
    }
}
