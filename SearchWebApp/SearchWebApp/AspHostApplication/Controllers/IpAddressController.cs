﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspHostApplication.Controllers
{
    [Route("api/ip")]
    [ApiController]
    public class IpAddressController : ControllerBase
    {
        private ISearchService _searchService;

        public IpAddressController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        [Route("location")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DTO.Organization))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetSingleLocation([FromQuery] string ip)
        {
            if (string.IsNullOrEmpty(ip))
            {
                return BadRequest();
            }

            try{
                var searchResult = _searchService.IpV4Search(ip);

                return Ok(searchResult);
            }
            catch(ArgumentException)
            {
                return BadRequest();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }            
        }
    }
}
