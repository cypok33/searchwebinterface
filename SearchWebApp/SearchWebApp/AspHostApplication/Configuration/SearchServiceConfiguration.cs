﻿using Services.Interfaces;
using System.Reflection.Metadata;

namespace AspHostApplication.Configuration
{
    public class SearchServiceConfiguration : IPhysicalFileDbConfiguration
    {
        public static string SectionName => "SearchServiceConfiguration";

        public string PhysicalFileDbPath { get; set; }
    }
}
