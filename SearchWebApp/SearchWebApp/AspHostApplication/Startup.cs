using AspHostApplication.Configuration;
using DTO.Database;
using DTO.Database.Mapping;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;


using Services.Implementations;
using Services.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace AspHostApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var searchConfig = new SearchServiceConfiguration();
            Configuration.GetSection(SearchServiceConfiguration.SectionName).Bind(searchConfig);
            services.AddSingleton<IPhysicalFileDbConfiguration>(searchConfig);


            services.AddSingleton<IMemoryMapper, MemoryMapper>();
            services.AddSingleton<IDbAccessor, FileDB>();
            services.AddScoped<ISearchService, SearchService>();
        }

         public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            // explicitly rewrite all to index.html, except of api calls, scripts and styles
            app.UseRewriter(new RewriteOptions()                
                .AddRewrite(
                    @"^((?!(API|api|(.*js)|(.*css))).)*$",
                    "index.html",
                    skipRemainingRules: false));

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), @"ClientApp")),
                RequestPath = new PathString("")
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
