using NUnit.Framework;

using DTO.Helpers;

namespace DTO.UnitTests
{
    public class IpV4TranslationHelperTests
    {

        [Test]
        [TestCase((uint)1936450164, "115.107.226.116")]
        [TestCase((uint)1936513029, "115.108.216.5")]
        public void MapFromUintToString(uint raw, string stringified)
        {
            var fromRaw = IpV4TranslationHelper.Cast(raw);

            Assert.AreEqual(stringified, fromRaw);
        }

        [Test]
        [TestCase((uint)1936450164, "115.107.226.116")]
        [TestCase((uint)1936513029, "115.108.216.5")]
        public void MapFromStringToUint(uint raw, string stringified)
        {
            var fromString = IpV4TranslationHelper.Cast(stringified);

            Assert.AreEqual(raw, fromString);
        }

        [Test]
        [TestCase((uint)0, "0.0.0.0")]
        [TestCase((uint)0xFFFF_FFFF, "255.255.255.255")]
        public void MapBorders(uint raw, string stringified)
        {
            var fromString = IpV4TranslationHelper.Cast(stringified);

            Assert.AreEqual(raw, fromString);

            var fromRaw = IpV4TranslationHelper.Cast(raw);

            Assert.AreEqual(stringified, fromRaw);
        }

        [Test]
        [TestCase("256.256.256.256")]
        [TestCase("-1.0.0.0")]
        [TestCase("dasdfkj")]
        [TestCase("")]
        public void MapOutOfRangeStrings(string stringified)
        {
            var exception = Assert.Throws<System.FormatException>(() =>
                IpV4TranslationHelper.Cast(stringified));

            Assert.That(exception.Message, Is.EqualTo("An invalid IP address was specified."));
        }

        [Test]
        [TestCase(null)]
        public void MapNullString(string stringified)
        {
            var exception = Assert.Throws<System.ArgumentNullException>(() =>
                IpV4TranslationHelper.Cast(stringified));

            Assert.That(exception.Message, Is.EqualTo("Value cannot be null. (Parameter 'address')"));
        }
    }
}