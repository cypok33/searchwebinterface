﻿using Services.Interfaces;

namespace Common.Tests
{
    // for testing purposes only. In application common configutaion should implement IPhysicalFileDbConfiguration
    public class PhysicalFileDbConfiguration : IPhysicalFileDbConfiguration
    {
        private readonly string _path;

        public PhysicalFileDbConfiguration(string path)
        {
            _path = path;
        }

        public string PhysicalFileDbPath => _path;
    }
}
