﻿using System.Runtime.InteropServices;

namespace DTO.AllocOptimized
{
    [StructLayout(LayoutKind.Sequential)]
    public struct IpRangeIndex
    {
        public static readonly int Size = 12; // use implicit size defenition because some fields could appear during linking, but not from the raw file
                                              // it's also not affects memory allocation, cause it's stored in a 'structures type defenition table'

        public uint ip_from;           // начало диапазона IP адресов
        public uint ip_to;             // конец диапазона IP адресов
        public uint location_index;    // индекс записи о местоположении
    }
}
