﻿using System.Runtime.InteropServices;

namespace DTO.AllocOptimized
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CityIndex
    {
        public static readonly int Size = 4; // only index provided in source file, so it's unable to use sizeof here.

        public uint offset;
        public string city; // this fiels isn't filling by FillFromSpan(). Depending on perf settings it could be filled from associated 'location', or could not.
    }
}
