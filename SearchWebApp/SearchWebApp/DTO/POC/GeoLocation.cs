﻿namespace DTO
{
    public class GeoLocation
    {
        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}
