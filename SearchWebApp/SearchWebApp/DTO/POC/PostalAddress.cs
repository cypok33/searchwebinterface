﻿namespace DTO
{
    public class PostalAddress : Address
    {
        public string Postal { get; set; }
    }
}
