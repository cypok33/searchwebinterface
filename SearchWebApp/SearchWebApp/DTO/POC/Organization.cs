﻿namespace DTO
{
    public class Organization
    {
        public string OrganizationName { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public GeoLocation GeoLocation { get; set; }
    }
}
