﻿namespace DTO
{
    public class StorageHeader
    {
        public static int Size => 60;

        public int Version { get; set; }
        public string Name { get; set; }
        public ulong Timestamp { get; set; }
        public int Records { get; set; }
        public uint Offset_ranges { get; set; }
        public uint Offset_cities { get; set; }
        public uint Offset_locations { get; set; }
    }
}
