﻿using System;

namespace DTO.Database
{
    [Flags]
    public enum AllocationStrategy
    {
        Default = 0,
        LinkCitiesInstantly = 0b01
    }
}
