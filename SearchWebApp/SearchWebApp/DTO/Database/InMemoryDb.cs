﻿using System;

using DTO.AllocOptimized;

namespace DTO.Database
{
    public class InMemoryDb
    {
        private readonly IMemoryMapper _memoryMapper;
        private readonly AllocationStrategy _allocationStrategy;

        private Memory<byte> _locationsMemSpan;
        private StorageHeader _header;
        private CityIndex[] _cityIndices;
        private IpRangeIndex[] _ipRangeIndices;

        public CityIndex[] Cities => _cityIndices;
        public IpRangeIndex[] IpRanges => _ipRangeIndices;

        public InMemoryDb(
            IMemoryMapper memoryMapper,
            AllocationStrategy allocationStrategy = AllocationStrategy.Default)
        {
            _memoryMapper = memoryMapper;
            _allocationStrategy = allocationStrategy;
        }

        public void Init(Memory<byte> sequentialMemoryAllocation)
        {
            _header = _memoryMapper.FillFromSpan(new StorageHeader(), sequentialMemoryAllocation.Span.Slice(0, StorageHeader.Size));

            _locationsMemSpan = sequentialMemoryAllocation.Slice((int)_header.Offset_locations, LocationInfo.Size * _header.Records);

            _ipRangeIndices = new IpRangeIndex[_header.Records];
            for (var i = 0; i < _header.Records; i++)
            {
                _memoryMapper.FillFromSpan(
                    ref _ipRangeIndices[i],
                    sequentialMemoryAllocation.Span.Slice((int)(_header.Offset_ranges + i * IpRangeIndex.Size), IpRangeIndex.Size));
            }

            _cityIndices = new CityIndex[_header.Records];
            var linkCities = (_allocationStrategy & AllocationStrategy.LinkCitiesInstantly) != 0;
            for (var i = 0; i < _header.Records; i++)
            {
                _memoryMapper.FillFromSpan(
                    ref _cityIndices[i],
                    sequentialMemoryAllocation.Span.Slice((int)(_header.Offset_cities + i * CityIndex.Size), CityIndex.Size));

                if(linkCities)
                {
                    _cityIndices[i].city = GetCityName((int)_cityIndices[i].offset);
                }
            }
        }

        public string GetCityName(int offset)
        {
            return _memoryMapper.ExtractCityName(_locationsMemSpan.Span.Slice(offset, LocationInfo.Size));
        }

        public Organization ByOffset(int offset, string preloadedCityName = null)
        {
            return _memoryMapper.ExtractOrganization(
                _locationsMemSpan.Span.Slice(offset, LocationInfo.Size),
                preloadedCityName);
        }

        public Organization ByIndex(int index)
        {
            return ByOffset(index * LocationInfo.Size);
        }
    }
}
