﻿using System;

using DTO.AllocOptimized;


namespace DTO.Database
{
    // interface provided to have an ability to substitute instance depending of file structure by DI.
    public interface IMemoryMapper
    {
        StorageHeader FillFromSpan(StorageHeader header, Span<byte> span);

        CityIndex FillFromSpan(ref CityIndex cityIndex, Span<byte> span);

        IpRangeIndex FillFromSpan(ref IpRangeIndex ipRangeIndex, Span<byte> span);

        Organization ExtractOrganization(Span<byte> span, string preloadedCityName = null);

        string ExtractCityName(Span<byte> span);
    }
}
