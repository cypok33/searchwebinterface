﻿using System;
using System.Text;

using DTO.AllocOptimized;
using DTO.Helpers;

namespace DTO.Database.Mapping
{
    public class MemoryMapper : IMemoryMapper
    {
        private Encoding _encoding;

        public MemoryMapper(Encoding encoding = null)
        {
            _encoding = encoding ?? Encoding.ASCII;
        }

        public StorageHeader FillFromSpan(StorageHeader header, Span<byte> span)
        {
            header.Version = BitConverter.ToInt32(span.Slice(0, 4));
            header.Name = _encoding.GetString(span.Slice(4, 32));
            header.Timestamp = BitConverter.ToUInt64(span.Slice(36, 8));
            header.Records = BitConverter.ToInt32(span.Slice(44, 4));
            header.Offset_ranges = BitConverter.ToUInt32(span.Slice(48, 4));
            header.Offset_cities = BitConverter.ToUInt32(span.Slice(52, 4));
            header.Offset_locations = BitConverter.ToUInt32(span.Slice(56, 4));

            return header;
        }

        public CityIndex FillFromSpan(ref CityIndex cityIndex, Span<byte> span)
        {
            cityIndex.offset = BitConverter.ToUInt32(span.Slice(0, 4));
            return cityIndex;
        }

        public IpRangeIndex FillFromSpan(ref IpRangeIndex ipRangeIndex, Span<byte> span)
        {
            ipRangeIndex.ip_from = BitConverter.ToUInt32(span.Slice(0, 4));
            ipRangeIndex.ip_to = BitConverter.ToUInt32(span.Slice(4, 4));
            ipRangeIndex.location_index = BitConverter.ToUInt32(span.Slice(8, 4));

            return ipRangeIndex;
        }

        public Organization ExtractOrganization(Span<byte> span, string preloadedCityName = null)
        {
            var org = new Organization
            {
                OrganizationName = span.Extract(56, 32, _encoding, "org_"),
                PostalAddress = new PostalAddress
                {
                    Country = span.Extract(0, 8, _encoding, "cou_"),
                    Region = span.Extract(8, 12, _encoding, "reg_"),
                    Postal = span.Extract(20, 12, _encoding, "pos_"),
                    City = preloadedCityName ?? ExtractCityName(span),
                },
                GeoLocation = new GeoLocation
                {
                    Latitude = BitConverter.ToSingle(span.Slice(88, 4)),
                    Longitude = BitConverter.ToSingle(span.Slice(92, 4))
                }
            };

            return org;
        }

        public string ExtractCityName(Span<byte> span)
        {
            return span.Extract(32, 24, _encoding, "cit_");
        }
    }
}
