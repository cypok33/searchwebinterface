﻿using System;
using System.Text;

namespace DTO.Helpers
{
    public static class DtoFillingExtensions
    {
        public static string Extract(this Span<byte> span, int start, int length, Encoding encoding)
        {
            return encoding.GetString(span.Slice(start, length)).TrimEnd('\0');
        }

        public static string Extract(this Span<byte> span, int start, int length, Encoding encoding, string trimStart)
        {
            return span.Extract(start, length, encoding).TrimStart(trimStart.ToCharArray());
        }
    }
}
