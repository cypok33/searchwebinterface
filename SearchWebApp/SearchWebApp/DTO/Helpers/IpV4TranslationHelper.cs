﻿using System;
using System.Linq;
using System.Net;

namespace DTO.Helpers
{
    public class IpV4TranslationHelper
    {
        public static string Cast(uint address)
        {
            var bytes = BitConverter.GetBytes(address);
            string res = string.Join(".", bytes.Reverse());
            return res;
        }

        public static uint Cast(string address)
        {
            if (address == null)
            {
                throw new ArgumentNullException(nameof(address), "Value cannot be null.");
            }

            var add = IPAddress.Parse(address);
            var bytes = add.GetAddressBytes();
            var res = BitConverter.ToUInt32(bytes.Reverse().ToArray());
            return res;
        }
    }
}
