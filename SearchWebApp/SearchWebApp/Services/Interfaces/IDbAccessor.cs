﻿
using System.Collections.Generic;

using DTO;

namespace Services.Interfaces
{
    public interface IDbAccessor
    {
        bool IsReady { get; }
        void Init();
        IEnumerable<Organization> CitySearch(string city);
        IEnumerable<Organization> IpV4Search(string ipAddress);
    }
}
