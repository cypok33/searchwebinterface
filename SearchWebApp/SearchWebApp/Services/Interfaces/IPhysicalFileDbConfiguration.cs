﻿
namespace Services.Interfaces
{
    public interface IPhysicalFileDbConfiguration
    {
        string PhysicalFileDbPath { get; }
    }
}
