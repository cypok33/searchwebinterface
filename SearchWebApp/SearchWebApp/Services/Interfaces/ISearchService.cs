﻿using System.Collections.Generic;

using DTO;

namespace Services.Interfaces
{
    public interface ISearchService
    {
        IEnumerable<Organization> CitySearch(string city);
        Organization IpV4Search(string ipAddress);
    }
}
