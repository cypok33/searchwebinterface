﻿using System.Collections.Generic;
using System.Linq;

using DTO;
using Services.Interfaces;

namespace Services.Implementations
{
    public class SearchService : ISearchService
    {
        private readonly IDbAccessor _dbAccessor;
        public SearchService(IDbAccessor dbAccessor)
        {
            _dbAccessor = dbAccessor;
            if (!_dbAccessor.IsReady)
            {
                _dbAccessor.Init();
            }
        }

        public IEnumerable<Organization> CitySearch(string city)
        {
            return _dbAccessor?.CitySearch(city);
        }

        public Organization IpV4Search(string ipAddress)
        {
            return _dbAccessor?.IpV4Search(ipAddress).SingleOrDefault();
        }
    }
}
