﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using DTO;
using DTO.AllocOptimized;
using DTO.Database;
using DTO.Helpers;
using Services.Interfaces;

namespace Services.Implementations
{
    public class FileDB : IDbAccessor
    {
        private readonly IMemoryMapper _memoryMapper;
        private readonly IPhysicalFileDbConfiguration _config;
        private InMemoryDb _inMemoryDb;

        private bool _initialized = false;
        public bool IsReady => _initialized;

        public FileDB(
            IMemoryMapper memoryMapper,
            IPhysicalFileDbConfiguration config)
        {
            _memoryMapper = memoryMapper;
            _config = config;
        }

        public void Init()
        {
            var allocatedFile = AllocateAsMemoryRegion(_config.PhysicalFileDbPath);

            _inMemoryDb = new InMemoryDb(_memoryMapper);
            _inMemoryDb.Init(allocatedFile);

            _initialized = true;
        }

        public IEnumerable<Organization> CitySearch(string city)
        {
            var matches = BinaryRangeSearch(_inMemoryDb.Cities.AsSpan(), new OrderedIndexComparableOrgLink(city, _inMemoryDb));

            return matches.ToArray().Select(item => _inMemoryDb.ByOffset((int)item.offset, item.city));
        }

        public IEnumerable<Organization> IpV4Search(string ipAddress)
        {
            var castedAddress = IpV4TranslationHelper.Cast(ipAddress);

            // As defined in writeup (and in a sample file), Ip ranges a sorted such that they couldn't inersect. 
            // Than, each range could containt 1 or 0 elements, and return value could be a single Organization or null
            // instead or Organization[] as array.
            // But, to keep interface consistent, accessor will return an array, and caller code should define retrun value
            // as Organization? as a single nullable value. It's just a SingleOrDefault call.
            var matches = BinaryRangeSearch(_inMemoryDb.IpRanges.AsSpan(), new Ipv4ComparableOrgLink(castedAddress, _inMemoryDb));

            return matches.ToArray().Select(item => _inMemoryDb.ByIndex((int)item.location_index));
        }

        private Memory<byte> AllocateAsMemoryRegion(string path)
        {
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                if(!File.Exists(path))
                {
                    throw new ArgumentException($"File [{path}] not found");
                }

                if(fs.Length > int.MaxValue)
                {
                    throw new ArgumentException(($"File [{path}] is too long. It should be ${int.MaxValue} max, but it is ${fs.Length} bytes long."));
                }


                byte[] file = new byte[fs.Length];
                fs.Read(file, 0, (int)fs.Length);

                return file.AsMemory();
            }
        }

        private Span<T> BinaryRangeSearch<T>(Span<T> span, IComparable<T> comparable)
        {
            var initOccurence = span.BinarySearch(comparable);

            if (initOccurence < 0)
            {
                return span.Slice(0, 0);
            }

            var left = initOccurence;
            var right = initOccurence;

            while (left - 1 >= 0)
            {
                if (comparable.CompareTo(span[left - 1]) == 0)
                    left--;
                else
                    break;
            }
            while (right + 1 < span.Length)
            {
                if (comparable.CompareTo(span[right + 1]) == 0)
                    right++;
                else
                    break;
            }

            return span.Slice(left, right - left + 1);
        }

        private class OrderedIndexComparableOrgLink : IComparable<CityIndex>
        {
            private readonly string _key;
            private readonly InMemoryDb _dbLink;
            public OrderedIndexComparableOrgLink(string key, InMemoryDb dbLink)
            {
                _key = key;
                _dbLink = dbLink;
            }

            public int CompareTo(CityIndex other)
            {
                return string.Compare(string.Intern(_key), _dbLink.GetCityName((int)other.offset));
            }
        }

        private class Ipv4ComparableOrgLink : IComparable<IpRangeIndex>
        {
            private readonly uint _key;
            private readonly InMemoryDb _dbLink;
            public Ipv4ComparableOrgLink(uint key, InMemoryDb dbLink)
            {
                _key = key;
                _dbLink = dbLink;
            }

            public int CompareTo(IpRangeIndex other)
            {
                var k_inline = IpV4TranslationHelper.Cast(_key);
                var from_inline = IpV4TranslationHelper.Cast(other.ip_from);
                var to_inline = IpV4TranslationHelper.Cast(other.ip_to);

                if (_key < other.ip_from)
                {
                    return -1;
                }
                else if (_key > other.ip_to)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
