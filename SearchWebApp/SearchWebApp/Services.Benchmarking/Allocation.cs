﻿using BenchmarkDotNet.Attributes;

using Common.Tests;
using DTO.Database.Mapping;
using Services.Implementations;

namespace Services.Benchmarking
{
    public class Allocation
    {
        private readonly PhysicalFileDbConfiguration _dbConfig = new PhysicalFileDbConfiguration("geobase.dat");

        [Benchmark]
        public bool DbAllocation()
        {
            var db = new FileDB(new MemoryMapper(), _dbConfig);
            db.Init();

            return db != null;
        }
    }
}
