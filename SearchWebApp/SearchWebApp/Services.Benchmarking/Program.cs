﻿using BenchmarkDotNet.Running;

namespace Services.Benchmarking
{
    public class FileDBBench
    {
        /*
        // * Performance investigation result *

            BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19042.1165 (20H2/October2020Update)
            Intel Core i5-8600K CPU 3.60GHz (Coffee Lake), 1 CPU, 6 logical and 6 physical cores
            .NET SDK=5.0.300-preview.21258.4
              [Host]     : .NET Core 3.1.15 (CoreCLR 4.700.21.21202, CoreFX 4.700.21.21402), X64 RyuJIT
              DefaultJob : .NET Core 3.1.15 (CoreCLR 4.700.21.21202, CoreFX 4.700.21.21402), X64 RyuJIT
            
            |                     Method |      Mean |     Error |    StdDev |
            |--------------------------- |----------:|----------:|----------:|
            |               DbAllocation |  7.243 ms | 0.1329 ms | 0.1243 ms |    
            |   SearchByCityName_5_items | 151.73 μs |  0.482 μs |  0.451 μs | => search for 5 items takes 0.150 ms ~= 0.030 ms/op
            | SearchByIpAddresse_5_items |  65.05 μs |  0.431 μs |  0.360 μs | => search for 5 items takes 0.065 ms ~= 0.015 ms/op


            Seems like 0.03 ms fast enough for single search operation per web request. It will not take sufficient time realatively to all other processing.
            More than, search is lazy, and strings, allocated on each search operation are saved (and, first of all, the middle item of array).
            Accoring to search is binary, second search operation is faster than first until FileDB is a singleton.

            [Decision]: lazy search is better than instant allocation of huge number of strings during parsing DB from file.
            Then requirement about load database faster than 30 ms is satisfied.

            Investigation about Marshalling, fixed arrays, pointers, memory pinning and instant mapping to structures
            was finished with min loading time about 75 ms for instant allocating all strings for Organization (i.e. name, region, etc.).
            The ugly code, produced by the investigation, is not included into this repo, because it's really ugly. You can see a pretty
            one version only for lazy loading here.
            Possible, I just don't know how to do these string allocations fast enough... But I solved to keep data in a Memory span and
            allocate strings for exact organization on call, e.g. 'lazy', till search is fast enough. Anyway, even in Memory region,
            database is 'in memory'. Do not hate me for this trick, I do my best! And, sure, I'm extremely want to know the way to
            allocate all strings instanly, if this way is exists.
            
         */

        public class Program
        {
            public static void Main(string[] args)
            {
                var allocations = BenchmarkRunner.Run<Allocation>();
                var searches = BenchmarkRunner.Run<Search>();
            }
        }
    }
}
