﻿using System.Collections.Generic;
using System.Linq;

using BenchmarkDotNet.Attributes;

using Common.Tests;
using DTO;
using DTO.Database.Mapping;
using Services.Implementations;

namespace Services.Benchmarking
{
    public class Search
    {
        private readonly PhysicalFileDbConfiguration _dbConfig = new PhysicalFileDbConfiguration("geobase.dat");

        private readonly string[] _citySearchList = new string[] { "A ", "Ixiluza", "A Adapo Ade Ar X", "Yzyrinorijyqotok", "Yzi" };
        private readonly string[] _ipSearchList = new string[] { "0.5.236.229",  "0.7.171.255", "0.8.233.247", "127.0.0.0", "231.175.71.65" };

        private FileDB _database;

        [GlobalSetup]
        public void GlobalSetup()
        {
            _database = new FileDB(new MemoryMapper(), _dbConfig);
            _database.Init();
        }

        [GlobalCleanup]
        public void GlobalCleanup()
        {
            _database = null;
        }

        [Benchmark]
        public bool SearchByCityName_5_items()
        {
            var results = new List<Organization[]>();

            foreach (var name in _citySearchList)
            {
                var searchResults = _database.CitySearch(name).ToArray();
                results.Add(searchResults);
            }

            return results.Count == _citySearchList.Length;
        }

        [Benchmark]
        public bool SearchByIpAddresse_5_items()
        {
            var results = new List<Organization[]>();

            foreach (var address in _ipSearchList)
            {
                var searchResults = _database.IpV4Search(address).ToArray();
                results.Add(searchResults);
            }

            return results.Count == _ipSearchList.Length;
        }
    }
}
