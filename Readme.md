# Current project state

## Deployed application sample
Container, deployed on AWS, provided here:

http://3.129.207.234/

NOTE!: push an issue if it isn't available. Possible, new version deploying.

## Build

### Prerequiesties
* npm 6.14.5 or later
* net5.0.0 or later  

### Production
* build JS app
    from 'SearchWebApp\ClientApp'
    - npm install
    - npm run build-prod
* build .net app in release mode


* from '\SearchWebApp\AspHostApplication\bin\Release\net5.0'
    - dotnet AspHostApplication.dll
* or run debug from .net IDE whatever you likes to use, with AspHostApplication debug configuration.
   NOTE! Described run instructions are provided of in-process .net core runtime.
   See https://bitbucket.org/cypok33/searchwebinterface/src/master/SearchWebApp/SearchWebApp/AspHostApplication/Properties/launchSettings.json if you want to deploy local IIS Express, or debug from deployed container (installed Docker 20.10.8 or later required - version doesn't depend for sure, it's just a simple linux container. Do not forget to map ports during creation a container from image.).
* Enjoy! Application is available at http://localhost:5000/

## What is done

* Filebase loading time. Takes less than 8 ms 'on my computer'. It is less than 50 on most part of weak machines, I hope. See comment at https://bitbucket.org/cypok33/searchwebinterface/src/WebHostPoc/SearchWebApp/SearchWebApp/Services.Benchmarking/Program.cs for details.
* Frontend made as 'SPA on JS'. It isn't pretty enough, and seems like 'selfmade Angular'. Looks like it was better to use TS instead of JS for such code organization.
* There is a 'mulisorting table'. Implementing of it is out of scope, but looks like I want to publish a package with it.

## Next steps 
~~What wasn't done, but wanted to be done~~

* Latitude and longitude are not showed on UI because there is a planned 'Show on map' component - a modal with OSM map with marker at location.
* Frontend codebase requires some refactoring (will be done, but it takes a time).
* There is not workaround for failed search requests.
* Some spinner needed to cover search results component during xnr request.
* CORS not managed at all.
* Styling of frontend app made for desctop widescreens, adaptive styles are not provided.
* Explicit language selector is not provided. i18n of the app depends on browser locale.
* Light/Dark theme switching is not provided, but all styles are made with CSS variables. It'll be simple to add it.
* HTTPS is disabled, sertificate is not provided for deployed app.
* UI looks ugly for me. It is too green and not green enough at the same time, too huge and too small, etc. Unfortunately, I have no artistic taste.
* Code almost doesn't contain comments. Most of all, there are provided only in places, which was not so simple for me.
* Multisorting should be provided in NPM as a package, if there is not analogs.